libemail-mime-contenttype-perl (1.028-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.028.
  * Update years of packaging copyright.
  * Update upstream email address.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Jan 2023 00:26:14 +0100

libemail-mime-contenttype-perl (1.027-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libemail-mime-contenttype-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 18:41:01 +0000

libemail-mime-contenttype-perl (1.027-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.027.
  * debian/copyright: update Upstream-Contact email address.
  * Update list of copyright holders for debian/*.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 Sep 2022 23:54:29 +0200

libemail-mime-contenttype-perl (1.026-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.026.
    - Don't use more memory than necessary to store parts of an encoded
      parameter (Closes: #960158)
  * Declare compliance with Debian policy 4.5.1

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 12 Jan 2021 06:54:14 +0100

libemail-mime-contenttype-perl (1.024-1) unstable; urgency=medium

  * Team upload

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Joenio Marques da Costa ]
  * Import upstream version 1.024
  * Add myself to d/copyright
  * using uscan special strings on d/watch
  * d/control: depends on libtext-unidecode-perl

  [ gregor herrmann ]
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- Joenio Marques da Costa <joenio@joenio.me>  Fri, 06 Nov 2020 22:59:43 +0100

libemail-mime-contenttype-perl (1.022-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Import upstream version 1.022
  * Update copyright years
  * Bump dh compat to level 9
  * Drop versioned dependency on perl satisfied in oldoldstable
  * Declare compliance with Debian Policy 4.1.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 18 Sep 2017 23:09:35 +0200

libemail-mime-contenttype-perl (1.018-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Import upstream version 1.018
  * Add upstream metadata
  * Update years of packaging copyright
  * Drop obsolete build-dependency on Capture::Tiny
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Wed, 12 Aug 2015 21:10:12 +0200

libemail-mime-contenttype-perl (1.017-1) unstable; urgency=low

  [ Brian Cassidy ]
  * New upstream release
  * debian/control:
    + added myself to Uploaders
  * debian/rules: use new template
  * debian/copyright: use new spec

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 1.017
  * Add build-dependencies on Capture::Tiny and Test::More 0.96 while dropping
    Test::Pod and Test::Pod::Coverage
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0, directly
    link GPL-1+)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Switch to source format 3.0 (quilt)
  * Switch to short-form debian/rules
  * Stop shipping README
  * Improve short and long description (noun phrase)
  * Update Homepage to point to Metacpan
  * Add myself to Uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Tue, 20 Aug 2013 21:37:07 +0200

libemail-mime-contenttype-perl (1.014-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists (Closes: #467828).

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to debhelper 6

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 14 Jan 2008 10:47:25 -0430

libemail-mime-contenttype-perl (1.014-2) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Moved package into Debian Pkg Perl Project SVN.
    + Added myself to Uploaders
  * Fixed copyright file with a better URL.
  * Cleaned up debian/rules.
  * Fixed watch file.
  * Added Build-Depends-Indep on libtest-pod-perl and
    libtest-pod-coverage-perl.
  * Fixed copyright and control file with a better URL.

  [ Damyan Ivanov ]
  * Use less bandwidth-hungry location in debian/watch
  * Use Homepage as stated by README
  * Don't install redundant README

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Fri, 23 Nov 2007 13:10:31 -0400

libemail-mime-contenttype-perl (1.014-1) unstable; urgency=low

  * New upstream release (Closes: #436609).

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 02 Jul 2007 11:12:05 -0400

libemail-mime-contenttype-perl (1.012-1) unstable; urgency=low

  * New upstream release.
  * Cleanup debian/rules.
  * Updated Standards Version.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Fri, 03 Nov 2006 18:37:27 -0400

libemail-mime-contenttype-perl (1.01-1) unstable; urgency=low

  * Initial Release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 05 Dec 2005 10:51:13 -0400
